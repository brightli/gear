﻿package gear.gui.core {
	/**
	 * @author bright
	 */
	public final class GAutoSizeMode {
		public static const NONE:int=0;
		public static const AUTO_SIZE:int=1;
		public static const AUTO_WIDTH:int=2;
		public static const AUTO_HEIGHT:int=3;
	}
}
