﻿package gear.gui.core {
	/**
	 * 颜色
	 * 
	 * @author bright
	 * @version 20130106
	 */
	public class GColor {
		//红色
		public static const RED : uint = 0xFF0000;
		//绿色
		public static const GREEN : uint = 0x009900;
		//蓝色
		public static const BLUE : uint = 0x0066FF;
		//青色
		public static const CYAN : int = 0x00FFFF;
		//黄色
		public static const YELLOW : int = 0xFFFF00;
		//白色
		public static const WHITE : int = 0xFFFFFF;
		//灰色
		public static const GRAY : int = 0x808080;
		//黑色
		public static const BLACK : uint = 0x000000;
		//橙色
		public static const ORANGE:uint=0xFFA500;
		public static const LIGHT_RED : int = 0xFF6600;
		public static const LIGHT_GREEN : int = 0x90EE90;
		public static const LIGHT_BLUE : int = 0xADD8E6;
		public static const LIGHT_YELLOW : int = 0xFFFFE0;
	}
}
